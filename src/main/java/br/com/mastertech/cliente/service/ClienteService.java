package br.com.mastertech.cliente.service;

import br.com.mastertech.cliente.exception.ClienteNotFoundException;
import br.com.mastertech.cliente.model.Cliente;
import br.com.mastertech.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente create(Cliente c) {
        return clienteRepository.save(c);
    }

    public Cliente findById(Long id) {
        return clienteRepository
                .findById(id)
                .orElseThrow(() -> new ClienteNotFoundException(id));
    }
}
